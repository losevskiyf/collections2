package collections;

import java.util.Iterator;
import java.util.LinkedList;

public class HashSet<T> implements Set<T>, Iterable<T>{

    private int capacity = 16;
    private int size = 0;
    LinkedList<T>[] baskets =(LinkedList<T>[]) new LinkedList[capacity];
    double loadFactor = 0.75d;

    private void resize(){
        if(loadFactor * capacity <= size){
            capacity = capacity * 2;
            LinkedList<T>[] newBaskets = (LinkedList<T>[]) new LinkedList[capacity];
            for(LinkedList<T> list: baskets)
                if(list!=null) {
                    for (T t : list) {
                        int index = getIndex((Object) t);
                        if (newBaskets[index] == null)
                            newBaskets[index] = new LinkedList<T>();
                        newBaskets[index].add(t);
                    }
                }
            baskets = newBaskets;
        }
    }
    private int hash(Object key){
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    private int getIndex(Object key){
        return (capacity-1) & (hash(key));
    }

    @Override
    public boolean add(T elem) {
        resize();
        int index = getIndex(elem);
        if(baskets[index]==null){
            baskets[index] = new LinkedList<T>();
            baskets[index].add(elem);
            size++;
            return true;
        }
        else{
            for(T t: baskets[index]){
                if(hash(elem) == hash(t) && (elem == t) || (elem != null && elem.equals(t)))
                    return false;
            }
            baskets[index].add(elem);
            size++;
        }
        return true;
    }

    @Override
    public boolean contains(Object o) {
        int index = getIndex(o);
        if(baskets[index]==null) return false;
        else {
            for (T t: baskets[index]){
                if(hash(o) == hash(t) && (o == t) || (o != null && o.equals(t)))
                    return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private int index = -1;

            @Override
            public boolean hasNext() {
                return index<size-1;
            }

            @Override
            public T next() {
                index++;
                int counter = -1;
                for (int j=0; j<=index; j++){
                    for(LinkedList<T> list: baskets){
                        if(list!=null){
                            for (T t: list){
                                counter++;
                                if(counter==index) return t;
                            }
                        }
                    }
                }
                return null;
            }
        };
    }

    @Override
    public boolean remove(Object o) {
        int index = getIndex(o);
        if(baskets[index]==null) return false;
        else {
            for (T t: baskets[index]){
                if(hash(o) == hash(t) && (o == t) || (o != null && o.equals(t))) {
                    baskets[index].remove(o);
                    size--;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(LinkedList<T> list: baskets){
            if(list!=null){
                for (T t: list){
                    sb.append(t+",");
                }
            }
        }
        if (sb.length()>2)
            sb.deleteCharAt(sb.length()-1);
        sb.append("]");
        return sb.toString();
    }
}
