package collections;

import java.util.Iterator;
import java.util.Random;

public class SetTest {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        Random rand = new Random(21);
        System.out.println(set.isEmpty());
        for(int i=0; i<100; i++)
            set.add(rand.nextInt(1000));
        System.out.println(set);
        System.out.println(set.size());
        System.out.println(set.contains(53));
        Iterator<Integer> it = set.iterator();
        while (it.hasNext())
            System.out.print(it.next()+" ");
        System.out.println();
        set.remove(556);
        System.out.println(set.size());
        System.out.println(set);
        System.out.println(set.contains(260));
        set.remove(383);
        System.out.println(set.size());
        System.out.println(set);
    }
}
