package collections;

import java.util.Iterator;

public interface Set<T>{
    boolean add (T element);
    boolean contains (Object o);
    boolean isEmpty();
    Iterator<T> iterator();
    boolean remove(Object o);
    int size();
}
